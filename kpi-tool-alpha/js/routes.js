angular.module('kpi-tool').config(function($routeProvider) {
	$routeProvider.when('/projects', {
		templateUrl: '/templates/pages/projects/index.html'
	})
	.when('/employees', {
		templateUrl: '/templates/pages/employees/index.html'
	})
	.when('/', {
		templateUrl: '/templates/pages/home/index.html'
	})
	.otherwise({redirectTo: '/'});
});
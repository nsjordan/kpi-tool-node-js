(function(){
	var app = angular.module('kpi-tool', ['ngMaterial', 'ngMessages']);

//=========================SERVICES=================================================================================

	app.factory("projectService", ["$http", function($http, $q, $scope) {
		this.project = {
			customer: null,
			project: null,
			project_id: null,
			status: null,
			start_date: null,
			end_date: null,
			expected_expenses: null,
			fixed: null,
			sow_total: null
		};
		
		this.getProject = function() {

			return $http.get("/selectedProject").then(function(response) {
				return response.data[0];
			});
		};

		this.getActivities = function() {
			return $http.get("/activityRates").then(function(response) {
				return response.data;
			});
		};

		this.updateProject = function() {
			return this.project;
		};

		this.print = function(){
			console.log("project: ", project);
		};
		return this;

	}]);

//===========================FILTERS=================================================================================

app.filter('unique', function() {
	return function(data) {

	};
});

//==========================CONTROLLERS==============================================================================

	//This controller is for the "navigation tab" specifically, 
	//it is used in the main "kpi-tool.html" file to show and hide 
	//different aspects of the page based on the input to the navigation-tab
	app.controller('NavController', function($rootScope, $scope) {

		//By default (on load), the "showActivityRatesPage" is shown
		this.pageTitle = "Activity Rates";
		this.showActivityRatesPage = true;
		this.showEmployeePage = false;

		//Each button in the navigation tab is controlled by a function
		//The activityRatesBtn function ensure the page title is accurate,
		//and only the aspects of the activity rates page are shown.
		//The show page booleans are refferenced with the Angular ng-show
		//attributes in the "kpi-tool.html" page
		this.activityRatesBtn = function() {
			this.showEmployeePage = false;
			this.pageTitle = "Activity Rates";
			this.showActivityRatesPage = true;
			$rootScope.$broadcast('reset');
			$('.panel-collapse').collapse('hide');
		};

		//See above
		this.employeePageBtn = function() {
			this.showActivityRatesPage = false;
			this.pageTitle = "Employee Cost";
			this.showEmployeePage = true;
			$rootScope.$broadcast('reset');
			$('.panel-collapse').collapse('hide');
		};
	});

	//This controller is for the "project-dashboard.html" page.
	app.controller('DashboardController', function(projectService, $scope, $rootScope, $http) {

		//The dashboard displays information from a single selected project.
		//The dashboard also has the option to edit the projects info.
		//To ensure that accurate data is displayed, the project form 
		//only has access to a copy of the project object.
		//When a form is submitted, the original object copies the newly 
		//editted one.

		//This empty object shows all the fields a final project object will contain.
		//All but the last 3 attributes are pulled directly from the database.
		$scope.project = {
			customer: null,
			project: null,
			project_id: null,
			status: null,
			start_date: null,
			end_date: null,
			expected_expenses: null,
			fixed: null,
			sow_total: null,

			//These 3 attributes are calculated based on the neos_project_invoice table in the database.
			billed_expenses: null,
			nonBilled_expenses: null,
			total_expenses: null
		}

		//This variable is used to reset the project object to null values
		$scope.initial = angular.copy($scope.project)

		//These variables are used in the project table and are calculated from timesheet and activity rate data.
		$scope.totalHours;
		$scope.totalFees;
		$scope.totalLoadedCost;

		$scope.marginWithExpenses;

		$scope.avgBillRate = ($scope.totalFees/$scope.totalHours);
		$scope.avgHourlyCost;

		//This variable is used to control execution of queries in the $scope.$on('update', function(){})
		$scope.readyToUpdate = true

		//When 'reset' is broadcast (from the navigation tab) reset all the dashboard varables
		$scope.$on('reset', function() {
			$scope.project = angular.copy($scope.initial);
			$scope.showForm = false;
			$scope.showAddExpense = false;
			$scope.showDash = true;

			$scope.totalHours = 0;
			$scope.totalFees = 0;
			$scope.totalLoadedCost = 0;

			$scope.avgBillRate = 0;
			$scope.avgHourlyCost = 0;
		});

		//This event is fired when a change to an employees bill rate is made.
		//If a change is made totalFees and avgBillRate both need to be updated.
		$scope.$on('updateTableFees', function(event, args){
			$scope.totalFees = args.data;
			$scope.avgBillRate = ($scope.totalFees/$scope.totalHours)
			$scope.marginWithExpenses = (1 - (($scope.totalLoadedCost + $scope.project.nonBilled_expenses) / $scope.totalFees)) * 100
		});
		
		//This function handles the bulk of the work in GETting and displaying the
		//data in the project dashboard. This function fires when a project is selected
		//or changed.
		$scope.$on('update', function() {
			//Only fire the function when it is ready
			if ($scope.readyToUpdate == true) {
				//Set initial conditions
				$scope.readyToUpdate = false
				$scope.totalHours = 0;
				$scope.totalFees = 0;
				$scope.totalLoadedCost = 0;
				$scope.avgBillRate = 0;
				$scope.avgHourlyCost = 0;

				//Ensure that the dashboard is displayed on a change of project
				$scope.showDash = true;
				$scope.showForm = false;
				$scope.showAddExpense = false

				//Use projectService to handle the GET function.
				//Assign the variables with returned values.
				projectService.getProject().then(function(update) {
					$scope.project.customer = update.customer;
					$scope.project.project = update.project;
					$scope.project.project_id = update.project_id;
					$scope.project.status = update.status;
					$scope.project.start_date = new Date(update.start_date);
					$scope.project.end_date = new Date(update.end_date);
					$scope.project.expected_expenses = update.expected_expenses;
					$scope.project.fixed = update.fixed;
					$scope.project.sow_total = update.sow_total;

				});

				//Use projectService to get the timesheet activities associated
				//with the selected project.
				projectService.getActivities().then(function(update) {

					//Run through the retuned array and calculate table data
					for (var i = update.length - 1; i >= 0; i--) {
						$scope.totalHours += update[i].hours;
						$scope.totalFees += (update[i].rate * update[i].hours);
						$scope.totalLoadedCost += update[i].loaded_cost;
					}

					//Calculate table data
					$scope.avgHourlyCost = $scope.totalLoadedCost/$scope.totalHours;
					$scope.avgBillRate = $scope.totalFees/$scope.totalHours;

					//If nothing is returned, avoid division errors
					if (update.length == 0) {
						$scope.avgHourlyCost = 0
						$scope.avgBillRate = 0
					}

					//Copy the returned array
					$scope.activities = update;
					$scope.editRates = angular.copy($scope.activities);


					$scope.marginWithExpenses = (1 - (($scope.totalLoadedCost + $scope.project.nonBilled_expenses) / $scope.totalFees)) * 100
					
				});

				//GET the expenses for the project
				$http.get("/projectExpenses").then(function(response) {

					//Reset the variables for the expenses displayed in the dashboard.
					$scope.project.billed_expenses = 0;
					$scope.project.nonBilled_expenses = 0;
					$scope.project.total_expenses = 0;

					//Iterate through the returned array 
					for (var i = response.data.length - 1; i >= 0; i--) {
						//If the expense is indicated as billed and the amount is not null

						if (response.data[i].billed == "Yes" && response.data[i].expense_amount != null) {
							//Add to the billed and total expense variables
							$scope.project.billed_expenses += response.data[i].expense_amount;
							$scope.project.total_expenses += response.data[i].expense_amount;
						}
						//Else as long as the amount is not null
						else if (response.data[i].expense_amount != null) {

							//Add to the nonBilled and total expense variables
							$scope.project.nonBilled_expenses += response.data[i].expense_amount;
							$scope.project.total_expenses += response.data[i].expense_amount;
						}
					}

					//When the function has completed its last async call, acknowledge that it is ready to update.
					$scope.readyToUpdate = true;
				});
			}
		})
		
		//This variable is used in the project form
		$scope.edit = angular.copy($scope.project);

		//The aspects of the dashboard also have booleans for hiding on button presses.
		$scope.showDash = true;
		$scope.showForm = false;
		$scope.showAddExpense = false;

		//This back button is only displayed when in the "Edit" form.
		//On pressing the back button, the editted object is reset to
		//a copy of the original. This button also hides the "Edit" form,
		//ensures the "Add Expense" view is hidden, and shows the dashboard again.
		this.backBtn = function() {
			$scope.edit = angular.copy($scope.project);
			$rootScope.$broadcast('backBtn');

			$scope.showForm = false;
			$scope.showAddExpense = false;
			$scope.showDash = true;

			$http.get("/projectExpenses").then(function(response) {

				//Reset the variables for the expenses displayed in the dashboard.
				$scope.project.billed_expenses = 0;
				$scope.project.nonBilled_expenses = 0;
				$scope.project.total_expenses = 0;

				//Iterate through the returned array 
				for (var i = response.data.length - 1; i >= 0; i--) {
					//If the expense is indicated as billed and the amount is not null

					if (response.data[i].billed == "Yes" && response.data[i].expense_amount != null) {
						//Add to the billed and total expense variables
						$scope.project.billed_expenses += response.data[i].expense_amount;
						$scope.project.total_expenses += response.data[i].expense_amount;
					}
					//Else as long as the amount is not null
					else if (response.data[i].expense_amount != null) {

						//Add to the nonBilled and total expense variables
						$scope.project.nonBilled_expenses += response.data[i].expense_amount;
						$scope.project.total_expenses += response.data[i].expense_amount;
					}
				}
				$scope.marginWithExpenses = (1 - (($scope.totalLoadedCost + $scope.project.nonBilled_expenses) / $scope.totalFees)) * 100
			});
		};

		//The save button returns the user to the dashboard as well, 
		//however first it copies the editted object onto the project
		//to update the view and POSTs the update to the server.
		this.saveProjectBtn = function() {

			//Update the view before returning to dashboard
			$scope.project = angular.copy($scope.edit);

			//POST the update to the server
			$http.post("/projectEdits", {edits: $scope.project}).then(function(response) {});

			//Show the dashboard
			$scope.showForm = false;
			$scope.showAddExpense = false;
			$scope.showDash = true;
		};

		//Similar functionality as above
		this.addExpenseBtn = function() {
			if ($scope.project.project_id == null) {
				alert("Please select a project")
			}
			else {
				$scope.showDash = false;
				$scope.showForm = false;
				$scope.showAddExpense = true;
			}
		};

		//Similar functionality as above
		this.editBtn = function() {
			if ($scope.project.project_id == null) {
				alert("Please select a project")
			}
			else {
				$scope.edit = angular.copy($scope.project);
				$scope.showAddExpense = false;
				$scope.showDash = false;
				$scope.showForm = true;
			}
		};

	});
	
	//This controller manages the functionality of the accordion menu which displays the activity rates
	//for the selected project.
	app.controller('EditRatesPanelController', function($scope, $http, $rootScope, projectService) {

		//This can go away
		$http.get("/employees").then(function(response) {
			$scope.employees = response.data;
		});

		//Booleans
		$scope.edit = false;
		$scope.projectSelected = false;

		//The empty activities array
		//totalFees will need to be recalculated if a bill rate is changed.
		$scope.activities = [];
		$scope.totalFees;

		//This array stores the indexes of bill rates which have been altered
		$scope.changedRates = [];

		//On a 'reset' reset the initital variables
		$scope.$on('reset', function() {
			$scope.edit = false;
			$scope.projectSelected = false;

			$scope.activities = [];
			$scope.totalFees = 0;
			$scope.editRates = angular.copy($scope.activities);
		});

		//On 'update' use projectService to get the activities for the project.
		//Iterate over activities and find those with no employee cost assosiated
		//Add an error attribute that contains a string denoting the employee
		//is missing a cost for the date on the timesheet.
		$scope.$on('update', function() {
			projectService.getActivities().then(function(update) {
				$scope.activities = update;
				for (var i = $scope.activities.length - 1; i >= 0; i--) {
					if ($scope.activities[i].hourly_cost == null) {

						//Create a date object then stringify it from the date on the timesheet
						var date = new Date($scope.activities[i].date).toDateString();

						//Add the error attribute
						$scope.activities[i].error = "No employee cost found before \"" + date + "\"";
					};
				};
				//Copy the returned results
				$scope.editRates = angular.copy($scope.activities);
			});

			//Set booleans accordingly
			$scope.projectSelected = true;
			$scope.edit = false;
		});

		//This doesn't need to exist
		this.newActivity = {
			employee: "",
			activity: "",
			rate: null,
			fees: 0,
			hourlyCost: 0,
			plannedHours: 0
		};

		//The edit button enables the input box for changing an employees bill rate on a project.
		//If there is no project selected it prompts the user.
		this.editBtn = function() {
			if ($scope.projectSelected == true) {
				$scope.edit = !$scope.edit;
			}
			else {
				alert("Please select a project")
			}
			
		};

		//The save button updates the view, disables the input box and posts all the changes made
		this.saveBtn = function() {
			//Update the view
			$scope.activities = angular.copy($scope.editRates);
			$scope.edit = !$scope.edit;

			//Run through the array of indexes which have been changed
			for (var i = $scope.changedRates.length - 1; i >= 0; i--) {

				//Get the object that was changed using the index stored in changedRates, and assign it to activity.
				var activity = $scope.editRates[$scope.changedRates[i]]

				//POST the activity to the server.
				$http.post("/updateRate", {activity: activity}).then(function(response) {});

			}

			$scope.totalFees = 0
			for (var i = $scope.editRates.length - 1; i >= 0; i--) {
				$scope.totalFees += ($scope.editRates[i].rate * $scope.editRates[i].hours)
			}

			$rootScope.$broadcast('updateTableFees', {data: $scope.totalFees});
		};

		//Just reset the view and changedRates array
		this.cancelBtn = function() {
			$scope.editRates = angular.copy($scope.activities);
			$scope.edit = !$scope.edit;
			$scope.changedRates = [];
		};

		//This doesn't need to exist
		this.addBtn = function() {

			//This if statement validates that the necessary fields are filled out.
			if ((this.newActivity.employee !== "") && (this.newActivity.activity !== "") && (this.newActivity.rate !== null)) {

				//Push the new activity onto the this.project and the this.editRates object.
				//Pushing onto the editRates object ensures the update is displayed in the modal.

				//this.newActivity.employee = this.newActivity.employee.name;
				//this.newActivity.employee_id = this.newActivity.employee.employee_id;
				//this.newActivity.activity = this.newActivity.activity.activity;
				
				$scope.editRates.push(angular.copy(this.newActivity));

				//Reset the "newActivity" object
				this.newActivity = {
					employee: "",
					activity: "",
					rate: null,
					fees: 0,
					hourlyCost: 0,
					plannedHours: 0
				};
			};	
		};

		//The delete button function in the activity rates table finds the index in the model 
		//array of the "activity" object which it takes as a parameter and removes it, using splice,
		//from the array. Removing the object from the array is immediately reflected in the table
		//by Angular
		this.deleteBtn = function(activity) {
			var index = $scope.editRates.indexOf(activity);
			$scope.editRates.splice(index, 1);
		};

		//The "getIndexOnChange(activity)" function tracks changes made to the editRates array by logging the index 
		//of each changed element in the "changedRates" array. This helps to slim the number of "POST"s made to the database.
		this.getIndexOnChange = function(activity) {
			//find the index of the given activity
			var index = $scope.editRates.indexOf(activity);

			//if the given activity is not already in the "changedRates" array, push it's index to the array.
			if ($scope.changedRates.indexOf(index) == -1) {
				$scope.changedRates.push(index);
			}
		};

		//The updateBtn(activity) function finds the index of given activity in the $scope.editRates array,
		//removes that index from the $scope.changedRates array, then "POST"s the update to the database.
		//In changing an activity rate, $scope.totalFees also needs to be recalculated and sent through a 
		//$broadcast to the dashboard.

		this.updateBtn = function(activity) {
			$scope.totalFees = 0;
			//get the index of the given activity.
			var index = $scope.editRates.indexOf(activity);

			//if the index has been logged in changedRates remove it.
			var indexOfUpdate = $scope.changedRates.indexOf(index);
			if (indexOfUpdate != -1) {
				$scope.changedRates.splice(indexOfUpdate, 1);
			}

			//POST the updated activity object to the server which will update the value in the database.
			$http.post("/updateRate", {activity: activity}).then(function(response) {});

			//update the view.
			$scope.activities[index].rate = angular.copy($scope.editRates[index].rate);

			//calculate the new $scope.totalFees.
			for (var i = $scope.editRates.length - 1; i >= 0; i--) {
				$scope.totalFees += ($scope.editRates[i].rate * $scope.editRates[i].hours)
			}

			//$broadcast the new value of $scope.totalFees.
			$rootScope.$broadcast('updateTableFees', {data: $scope.totalFees});
		};

		//This.sorted is used to determine if the array has been sorted in ascending or descending order
		//When the value is "0", the sort function will sort in ascending order, when "1" in descending.
		this.sorted = 0;
		//This.lastPress allows the function to determine the last button pressed. This is used when the
		//array is sorted once by one attribute, and then by another. Without knowing that the last button 
		//pressed is different than the current one, on pressing the current button, the array would sort in 
		//descending order. E.g. on press of the "Employees" button, the array sorts by employees' names in
		//ascending order. If the second press is not the "Employees" button, the value of "this.sorted" will tell 
		//the function that it should sort in descending order.
		this.lastPress = 0;

		//The sortBy() function is extensible to any attribute of the project.activities/editRates.activities array.
		//It takes the attribute to sort by as well as a unique integer for each different button on which the sortBy()
		//function is called. On the first press of any button, the function will sort the array based on the given "attr"
		//parameter in ascending order. On each subsiquent, consecutive press of any one button, the function will toggle
		//between acsending and descending order. This is how you would expect the sort buttons to function.
		this.sortBy = function(attr, btn) {

			//This statement ensures that if a button is pressed for the first time consecutively, it will sort in 
			//acsending order according the associated attribute.
			if (this.lastPress !== btn) {
				this.sorted = 0;
			}

			//This.lastPress is then reassigned to the button which was pressed.
			this.lastPress = btn;

			//If the array has been sorted in ascending order for the given button
			if (this.sorted === 1) {

				//Reassign this.sorted to show the array has been sorted in descending order
				this.sorted = 0;

				//Use the built in JS function to sort the array: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort?v=example
				$scope.editRates.sort(function(a, b) {

					//For all attributes passed to the function, we can access them directly
					//as they are attributes of the "avtivities" object we are accessing.
					
					var A = a[attr];
					var B = b[attr];

					//If the attribute returned is a string, make sure values are comparable
					if (typeof(A) === "string") {
						A = A.toUpperCase();
						B = B.toUpperCase();
					}
					
					//Once the returned attributes are processed, sort descending.
					if (A > B) return -1;
					
					if (A < B) return 1;
					return 0;
				});
			}

			//If the array has not yet been sorted, OR not yet been sorted by the given "attr"
			//OR been sorted by the given "attr" AND the last press was the same button...
			else {

				//Reassign this.sorted to reflect that the array has been sorted by the given "attr"
				//in ascending order.
				this.sorted = 1;

				//Again use the built in JS sort function
				$scope.editRates.sort(function(a, b) {
					
					var A = a[attr];
					var B = b[attr];

					if (typeof(A) === "string") {
						A = A.toUpperCase();
						B = B.toUpperCase();
					}

					//Only change is the "<" and ">" below are reversed.
					if (A < B) {
						return -1;
					}
					if (A > B) {
						return 1;
					}
					return 0;
				
				});
			};
		};
	});

	//This controller is specific to the "edit-hours-modal.html"
	//This controller should take the same project object as the dashboard
	//(I.e. the currently selected project). The modal displays the activity rates
	//stored in the "activities" array of the current project. The modal allows for
	//activities to be removed and added from a list of all employees and activities
	//which populate the dropdowns from a separate array.
	app.controller('EditHoursModalController', function() {
		this.project = proj;

		//It is necessary to copy the current project to prevent two way binding
		//(Otherwise changes to the form will automatically change the object without input
		//from a submit button)
		this.editRates = angular.copy(this.project);

		//This is the array of currated employees which is iterated over for the employee
		//select dropdown.
		this.employees = emp;

		//Currently unused
		this.showPanel = true;

		//This blank activity object is linked to the "add" button on the modal to add
		//new activities to a projects "activities" array. The "newActivity" is filled out
		//using the dropdowns and the "Rate" input box. When the "add" button is pressed the 
		//object is pushed to the projects activities array, then reset.
		this.newActivity = {
			employee: "",
			activity: "",
			rate: 0,
			fees: 0,
			hourlyCost: 0,
			plannedHours: 0
		};

		//Currently unused
		this.editBtn = function() {
			this.showPanel = !this.showPanel;
		};

		//This function fires whenever the modal is hidden (e.g. whenever it is closed in any way)
		//It is supposed to reset the form so that editted rates that are not submitted are not
		//displayed after a close/reopen. Currently it seems the function does not have access
		//to the other objects in the controller and cannot reset form.
		$('#editHoursModal').on('hidden.bs.modal', function () {
			$(this).find("input").val('').end();
			this.editRates = angular.copy(proj);
		});

		//The ".addBtn()" is used to add a new activity to a projects "activities" array.
		//As descrribed above, when pressed, if the input is valid, the "newActivity"
		//object is pushed onto the "activities" array of the current project and
		//reset to empty, non-valid, values.
		this.addBtn = function() {

			//This if statement validates that the necessary fields are filled out.
			if ((this.newActivity.employee !== "") && (this.newActivity.activity !== "") && (this.newActivity.rate !== 0)) {

				//Push the new activity onto the this.project and the this.editRates object.
				//Pushing onto the editRates object ensures the update is displayed in the modal.
				this.project.activities.push(angular.copy(this.newActivity));
				this.editRates.activities.push(angular.copy(this.newActivity));

				//Reset the "newActivity" object
				this.newActivity = {
					employee: "",
					activity: "",
					rate: 0,
					fees: 0,
					hourlyCost: 0,
					plannedHours: 0
				};

				console.log("editRates.activities: ", this.editRates.activities)
				console.log("project.activities: ", this.project.activities)

			};
			
		};

		//The "deleteBtn()" function is used to remove an object from the "activities" array.
		//The function takes the current activity (the object it is printed next to when the
		//"activities" array is iterated over), determines its index and removes that index
		//(using the splice function) from the array. Angular then regenerates the list of items
		//on the page from the new array.
		this.deleteBtn = function(activity) {
			var index = this.editRates.activities.indexOf(activity);
			this.editRates.activities.splice(index, 1);

			console.log("editRates.activities: ", this.editRates.activities)
			console.log("project.activities: ", this.project.activities)
		};

		//Similar to the "deleteBtn()" function, the submit button finds the index of the object it is printed with.
		//Using that index it copies the rate which is editted in the "editRates" object and copies it to the "project"
		//object.
		this.submitBtn = function(activity) {
			var index = this.editRates.activities.indexOf(activity);
			this.project.activities[index].rate = angular.copy(this.editRates.activities[index].rate);

			console.log("rate ",this.project.activities[index].rate);

		};
	});

	//This controller governs the functionality of the "Change Project" tab. It is responsible for "GET"ting the
	//list of customers currated by status, and the list of projects currated by customer and status.
	//Once a project is selected the buttons send a $broadcast to the rest of the page to update accordingly.
	app.controller('ChangeProjectTabController', function($http, $scope, projectService, $rootScope) {

		//These booleans hide the dropdowns before they are currated
		$scope.hideCustomerSelect = true;
		$scope.hideProjectSelect = true;
		$scope.disableChangeBtn = true;

		//These variables are modeled to each dropdown
		$scope.selectedStatus = null;
		$scope.selectedCustomer = null;
		$scope.selectedProject = null;

		$scope.projects; //The array of currated projects
		$scope.currProject; //The final project object returned on button presses

/*
		$scope.$on('reset', function() {
			var element = document.getElementById('selectStatusDropdown')
			element.value = ""
			element = document.getElementById('selectCustomerDropdown')
			element.value = ""
			element = document.getElementById('selectProjectDropdown')
			element.value = ""
			$scope.selectedStatus = null;
			$scope.selectedCustomer = null;
			$scope.selectedProject = null;
			$scope.hideCustomerSelect = true;
			$scope.hideProjectSelect = true;
			$scope.disableChangeBtn = true;
			console.log("RESET DROPDOWNS")
		})
*/
		//This function uses the selected status to "GET" customers who have a project with a matching status.
		this.sendStatus = function() {
			$scope.hideCustomerSelect = false;
			$scope.hideProjectSelect = true;
			$scope.projects = null;
			$scope.currProject = null;

			$scope.selectedCustomer = null;
			$scope.selectedProject = null;

			//Send the selected status to the server to execute the query. The returned object is an array
			//of customers who have projects fitting the criteria.
			$http.post("/selectedStatus", {status: $scope.selectedStatus}).then(function(response) {
				//Once the POST has finished execute a GET to return the customers array
				$http.get("/curratedCustomers").then(function(response) {
					$scope.customers = response.data;
				});

			});
		};

		//This function sends the selected customers info to the server to execute a query on their projects.
		this.sendCustomer = function() {
			//When the dropdowns reset to null values this function fires because of ng-change.
			//This if statement ensure that the functionality only works when the dropdown value
			//selected is not null. E.g. it won't fire on reset to null, but will when a non null value is selected.
			if ($scope.selectedCustomer != null) {

				$scope.hideProjectSelect = false;
				$scope.currProject = null;
				$scope.selectedProject = null;

				//POST the selected customer and retrieve an array of their projects.
				$http.post("/selectedCustomer", {customer: $scope.selectedCustomer}).then(function(response) {
					$http.get("/curratedProjects").then(function(response) {
						$scope.projects = response.data;
					});
				});
			}
		}

		//When a project is selected and not null send the project info to the server to query the database
		this.sendProjectId = function() {
			if ($scope.selectedProject != null) {
				$http.post("/selectedProject", {project: $scope.selectedProject}).then(function(response) {
					//Set $scope.currProject to the retuned project object.
					$scope.currProject = projectService.getProject();
				});
			}
		}

		//The changeProjectBtn() tells the rest of the view to update if a project is selected.
		this.changeProjectBtn = function() {
			if($scope.currProject) {
				$rootScope.$broadcast('update');
				$('.panel-collapse').collapse('hide');
			}
			else {
				console.log("change project button disabled")
			}
		};

		//The nextBtn() function iterates through the $scope.projects array.
		this.nextBtn = function() {
			//If there is a project selected...
			if ($scope.selectedProject != null) {
				var index;
				//Find the index of the $scope.selectedProject in $scope.projects
				for (var i = $scope.projects.length - 1; i >= 0; i--) {
					if ($scope.projects[i].project_id == $scope.selectedProject.project_id) {
						index = i;
					}
				}

				//Increment index to get the index of the next project
				var indexOfNextProject = index + 1;
				//If the current project is the last in the array, start from the begining
				if (indexOfNextProject == $scope.projects.length) {
					indexOfNextProject = 0;
				}

				//Get the next project from the project array
				var nextProject = $scope.projects[indexOfNextProject];
				$scope.selectedProject = angular.copy(nextProject);

				//POST the next project to change the server-side variables
				$http.post("/selectedProject", {project: nextProject}).then(function(response) {
					
					//update the current project
					$scope.currProject = projectService.getProject();

					//Notify the rest of the view of the update
					if($scope.currProject) {
						$rootScope.$broadcast('update');
						$('.panel-collapse').collapse('hide');
						$scope.selectedProject = nextProject;
					}
					else {
						console.log("next btn disabled")
					}
				});
			}

			else {
				console.log("next btn disabled")
			}
		}

		//The previousBtn() function is almost identical to the nextBtn() function except that it decrements the index.
		this.previousBtn = function() {
			if ($scope.selectedProject != null) {
				var index;

				for (var i = $scope.projects.length - 1; i >= 0; i--) {
					if ($scope.projects[i].project_id == $scope.selectedProject.project_id) {
						index = i;
					}
				}

				var indexOfPreviousProject = index - 1;
				if (indexOfPreviousProject == -1 || index == -1) {
					indexOfPreviousProject = $scope.projects.length - 1;
				}

				var previousProject = $scope.projects[indexOfPreviousProject];

				$http.post("/selectedProject", {project: previousProject}).then(function(response) {
					$scope.currProject = projectService.getProject();
				
					if($scope.currProject) {
						$rootScope.$broadcast('update');
						$('.panel-collapse').collapse('hide');
						$scope.selectedProject = previousProject
					}
					else {
						console.log("prev btn disabled")
					}	
				});

			}

			else {
				console.log("prev btn disabled")
			}
		}
		
	});
	
	//This controller is responsible for viewing and adding employee loaded costs
	app.controller('EmployeePageController', function($http, $scope) {

		//This boolean is used to hide the table if there are no loaded costs for an employee
		$scope.showTable = false;

		//This is an array of employee objects
		$scope.employees;

		$scope.makeEdits = false;

		//An object to store the selected employee
		$scope.selectedEmployee = 
		{
			name: null,
			employee_id: null
		};

		//An array of all costs associated with an employee
		$scope.employeeCosts =
		[{
			employee_id: null,
			loaded_cost: null,
			begin_date: null
		}];

		//An object to store a new cost for an employee
		$scope.newCost = 
		{
			employee_id: null, 
			loaded_cost: null, 
			begin_date: new Date()
		};

		//The name displayed at the top of the page
		$scope.displayName;

		//Reset all the variable on a 'reset' call.
		//This ensures that the displayed data is wiped when leaving and returning to the page.
		$scope.$on('reset', function() {
			
			$scope.showTable = false;
			$scope.makeEdits = false;
			$scope.selectedEmployee = 
			{
				name: null,
				employee_id: null
			};

			$scope.employeeCosts =
			[{
				employee_id: null,
				loaded_cost: null,
				begin_date: null
			}];

			$scope.newCost = 
			{
				employee_id: null, 
				loaded_cost: null, 
				begin_date: new Date()
			};

			$scope.displayName = null;
		});

		//This function is used to fill the objects in the employee page when an employee is selected.
		//On a call to this function the database is queried for all the costs associated with an employee.
		this.changeBtn = function() {
			//If an employee has been selected
			if ($scope.selectedEmployee.employee_id != null) {
				//Clear the loaded_cost attribute
				$scope.newCost.loaded_cost = null;
				//update displayName to the selected employee
				$scope.displayName = angular.copy($scope.selectedEmployee.name);
				//POST the employees info to the server
				$http.post("/selectedEmployee", {employee_id: $scope.selectedEmployee.employee_id}).then(function(response) {
					
					//GET the costs from the database
					$http.get("/employeeCosts").then(function(response) {

						//Set the employee_id for a newCost to the correct value
						$scope.newCost.employee_id = angular.copy($scope.selectedEmployee.employee_id);

						//If no costs are returned
						if (response.data.length == 0) {
							//Don't display the table
							$scope.showTable = false;
							//Reset the view
							$scope.employeeCosts = [];
							
						}

						//Else, if there are rows returned
						else {
							//Show the table
							$scope.showTable = true;
							//Set costs equal to the array returned by the GET
							$scope.employeeCosts = response.data;
						}
					});
				});
			}
			//If there is no employee selected don't do anything
			else {
				console.log("change btn disabled")
			}
		};

		//The nextBtn() function iterates through the employees array and pulls the loaded
		//cost for each employee as it goes.
		this.nextBtn = function() {

			//First get the index of the current employee in the employees array
			var index = $scope.employees.indexOf($scope.selectedEmployee)

			//Increment by 1 to get the index of the next employee
			var indexOfNextEmployee = index + 1

			//Ensure that there is no index out of bounds error by
			//starting from the first employee after reaching the last
			if (indexOfNextEmployee == $scope.employees.length) {
				indexOfNextEmployee = 0
			}

			//Change selected employee to the next indexed employee
			$scope.selectedEmployee = $scope.employees[indexOfNextEmployee];
			$scope.displayName = angular.copy($scope.selectedEmployee.name);

			//Send the newly selected employee_id to the server
			$http.post("/selectedEmployee", {employee_id: $scope.selectedEmployee.employee_id}).then(function(response) {

				//GET the costs associated with the new employee
				$http.get("/employeeCosts").then(function(response) {

					//Reset the newCost object
					$scope.newCost.employee_id = angular.copy($scope.selectedEmployee.employee_id);
					$scope.newCost.begin_date = new Date();
					$scope.newCost.loaded_cost = null;

					if (response.data.length == 0) {
						$scope.showTable = false;
						$scope.employeeCosts = [];
					}
					else {
						$scope.showTable = true;
						$scope.employeeCosts = response.data;
					}
				});
			});
		}

		//Almost identical functionality to the nextBtn() function
		this.previousBtn = function() {
			var index = $scope.employees.indexOf($scope.selectedEmployee)
			var indexOfPreviousEmployee = index - 1

			if (indexOfPreviousEmployee == -1 || index == -1) {
				indexOfPreviousEmployee = $scope.employees.length - 1
			}

			$scope.selectedEmployee = $scope.employees[indexOfPreviousEmployee];
			$scope.displayName = angular.copy($scope.selectedEmployee.name);

			$http.post("/selectedEmployee", {employee_id: $scope.selectedEmployee.employee_id}).then(function(response) {

				$http.get("/employeeCosts").then(function(response) {
					$scope.newCost.employee_id = angular.copy($scope.selectedEmployee.employee_id);
					$scope.newCost.begin_date = new Date();
					$scope.newCost.loaded_cost = null;

					if (response.data.length == 0) {
						$scope.showTable = false;
						$scope.employeeCosts = [];
					}
					else {
						$scope.showTable = true;
						$scope.employeeCosts = response.data;
					}
				});
			});
		}

		//The add button adds new employee costs to an employee
		this.addBtn = function() {
			//Only execute the function if an employee is selected and a loaded_cost is input
			if($scope.newCost.employee_id != null && $scope.newCost.loaded_cost != null) {

				//This variable is used to notify the user if an entry already exists with the same
				//primary key (date)
				var primaryKeyError = false;

				//Iterate through the array of employee costs and check if the input date already
				//exists in the database. If it does, alert the user, break the loop and set the
				//error variable to true
				for (var i = $scope.employeeCosts.length - 1; i >= 0; i--) {
					if (new Date($scope.employeeCosts[i].begin_date).toDateString() == $scope.newCost.begin_date.toDateString()) {
						primaryKeyError = true;
						alert("The selected date, " + "\"" + $scope.newCost.begin_date.toDateString() + "\"" + " already has an associated cost.")
						break;
					};
				};

				//If there is no primary key error, post the newCost object to the server,
				//reset the newCost object, and show the table of employee costs.
				if (primaryKeyError == false) {
					$http.post("/newCost", {newCost: $scope.newCost}).then(function(response) {
						$scope.employeeCosts[$scope.employeeCosts.length] = angular.copy($scope.newCost);
						$scope.newCost.begin_date = new Date();
						$scope.newCost.loaded_cost = null;
					});
					$scope.showTable = true;
				};
			}
			else {
				alert("Please select an employee and input a new cost.")
			}
		};

		this.editBtn = function() {
			$scope.makeEdits = true;
		};

		//The delete button takes a cost to delete, sends the cost object to the server for removal.
		//It also finds the index of the cost in the costs array and removes it from the view.
		this.deleteBtn = function(cost) {
			$http.post("/deleteCost", {cost: cost}).then(function(response) {});
			var index = $scope.employeeCosts.indexOf(cost);
			$scope.employeeCosts.splice(index, 1);
		};

		//GET the employees array from the database
		$http.get("/employees").then(function(response) {
			$scope.employees = response.data;
		});
	});

	//This controller handles adding, removing and displaying expenses for selected projects.
	app.controller('AddExpenseController', function($http, $scope) {

		//Empty variables for the array of expenses associated with a project
		//and the newExpense object
		$scope.expenses = [];
		$scope.newExpense = {
			expense_amount: null,
			expense_description: null,
			expense_date: new Date(),
			billed: null,
			project_id: null
		};

		//The object which an expense will be copied to when the edit button is pushed
		$scope.editExpense;

		//Keep track of the project_id to attach to new expenses
		$scope.project_id = null;

		//Reset all the variables when returning to the dashboard
		$scope.$on('backBtn', function() {
			$scope.newExpense = {
				expense_amount: null,
				expense_description: null,
				expense_date: new Date(),
				billed: null,
				project_id: $scope.project_id
			};
		});

		//Reset variables on update and GET the array of expenses
		$scope.$on('update', function() {
			$scope.expenses = [];
			$scope.newExpense = {
				expense_amount: null,
				expense_description: null,
				expense_date: new Date(),
				billed: null,
				project_id: null
			};

			//GET the expenses array
			$http.get("/projectExpenses").then(function(response) {
				
				//If there is data returned, copy it to the expenses array
				if (response.data[0].none == false) {
					$scope.expenses = response.data;
				}
				$scope.project_id = response.data[0].project_id;
				$scope.newExpense.project_id = response.data[0].project_id

			});
		});


		//The save button pushed the newExpense object to the server. It also tracks the attributes
		//of the newExpense object and won't allow null values to be pushed.
		this.saveBtn = function() {

			//"denied" is a boolean which either allows the object to be sent to the server or not.
			//"nulls" is an array to which null attributes in the newExpense object are pushed.
			var denied = false;
			var nulls = [];

			//This loop checks the attributes if the newExpense object and adds the attributes
			//names which have null values to the "nulls" array. It also sets "denied" to true
			//if there are any null attributes.
			for (var i in $scope.newExpense) {
				if($scope.newExpense[i] == null) {
					denied = true;
					nulls.push(i);
				}
			}

			//If "denied" is true, notify the user of the missing attributes
			if (denied == true) {
				alert("Please enter values in the following field(s): \n" + nulls)
			}

			//Else if the newExpense object has been verified POST it to the server.
			//Update the expenses array with a new GET request on completion of the POST.
			//Reset the new Expense object
			else {
				$http.post("/newExpense", {expense: $scope.newExpense}).then(function(response) {
					$http.get("/projectExpenses").then(function(response) {
						$scope.expenses = response.data;
					})
				});
				$scope.newExpense = {
					expense_amount: null,
					expense_description: null,
					expense_date: new Date(),
					billed: null,
					project_id: $scope.project_id
				};
			}
		};

		//Deletes an expense from the project by POSTing it to the server for deletion.
		//Also remove the expense from the view.
		this.deleteBtn = function(expense) {
			$http.post("/deleteExpense", {expense: expense}).then(function(response) {});
			var index = $scope.expenses.indexOf(expense);
			$scope.expenses.splice(index, 1);
		};

		//This function only copies the expense onto the editExpense object.
		//It also converts the expense_date string back to a date object for the md-datepicker.
		//The view on an edit button press is changed in the "add-expense.html" file by use of
		//a modal. To launch the modal the button is given data-toggle and data-target attributes.
		this.editBtn = function(expense) {
			$scope.editExpense = angular.copy(expense)
			$scope.editExpense.expense_date = new Date($scope.editExpense.expense_date);
		};

		//This button exists in the edit expense modal. It functions almost identically to
		//the above saveBtn, however it pushes the object to the "/updateExpense" URL and
		//closes the modal on press if all fields are filled.
		this.saveChangesBtn = function() {
			var denied = false;
			var nulls = [];
			for (var i in $scope.editExpense) {
				if($scope.editExpense[i] == null) {
					denied = true;
					nulls.push(i);
				}
			}
			if (denied == true) {
				alert("Please enter values in the following field(s): \n" + nulls)
			}
			else {

				$('#editExpenseModal').modal('hide');

				$http.post("/updateExpense", {expense: $scope.editExpense}).then(function(response) {
					$http.get("/projectExpenses").then(function(response) {
						$scope.expenses = response.data;
					})
				});
			}
		};
	});
	
	
	

	//==================================	DIRECTIVES		===========================================================================================

	//Each directive below creates a custom html tag for each of the html files in the project.
	//The tags name is described by the argument inside the directive. The argument is then converted to camelCase
	//e.g. 'navigationTab' becomes '<navigation-tab></navigation-tab>' and displays the content saved in 'navigation-tab.html'
	//NOTE: The name of the tag is not dependant AT ALL on the name of the "templateUrl" returned by the directive.

	app.directive('navigationTab', function() {
		return {
			restrict: "E",
			templateUrl: 'navigation-tab.html'
		};
	});

	app.directive('changeProjectTab', function() {
		return {
			restrict: "E",
			templateUrl: 'change-project-tab.html'
		};
	});

	app.directive('activityRatesPanel', function() {
		return {
			restrict: "E",
			templateUrl: 'activity-rates-panel.html'
		};
	});

	app.directive('projectDashboard', function(){
		return {
			restrict: 'E',
			templateUrl: 'project-dashboard.html'
		};
	});

	app.directive('projectForm', function(){
		return {
			restrict: 'E',
			templateUrl: 'project-form.html'
		};
	});

	app.directive('projectTable', function(){
		return {
			restrict: 'E',
			templateUrl: 'project-table.html'
		};
	});

	app.directive('actualProjectedHours', function(){
		return {
			restrict: 'E',
			templateUrl: 'actual-projected-hours.html'
		};
	});

	app.directive('editHoursModal', function(){
		return {
			restrict: 'E',
			templateUrl: 'edit-hours-modal.html'
		};
	});

	app.directive('addExpense', function(){
		return {
			restrict: 'E',
			templateUrl: 'add-expense.html'
		};
	});

	app.directive('changeEmployeeTab', function(){
		return {
			restrict: 'E',
			templateUrl: 'change-employee-tab.html'
		};
	});

	app.directive('employeePagePanel', function(){
		return {
			restrict: 'E',
			templateUrl: 'employee-page-panel.html'
		};
	});

	app.directive('editRatesPanel', function(){
		return {
			restrict: 'E',
			templateUrl: 'edit-rates-panel.html'
		};
	});

	app.directive('activityRatesAccordion', function(){
		return {
			restrict: 'E',
			templateUrl: 'activity-rates-accordion.html'
		};
	});

	app.directive('employeeSelector', function(){
		return {
			restrict: 'E',
			templateUrl: 'employee-selector.html'
		};
	});

//==================== TEST VARIABLES/OBJECTS ================================================================================================

	//This array is representative of the currated employees list which will be pulled from the database,
	//currated and used in the employee select dropdown. Eventually this array should contain all employees
	//whose "activity status" in the database is set to "active." This allows management to easily add or remove
	//employees from this list using the "Orange" DB's GUI. While this will display employees with a "termination ID,"
	//it can be easily currated, and allows management to easily currate the list.
	var emp = ["Noah Jordan", "Noah Gordan", "Noah Bordan", "Noah Fordan", "iludiuglfwiuglfiugleiugweiufgweuig"]

	//This object respresents the currated object built for each project. Eventually these objects will be stored in 
	//an array and iterated over for the project selection dropdown. As of now, this object may need more attributes.
	//The "start" and "end" attributes need to be date objects for the Angular calendars to correctly display and edit them.
	//The "activities" array is a list of activities associated with each project. The array can be editted in the edit hours
	//modal.
	var proj = {
		name: "The test project",
		customer: "A test company",
		isFixed: "Yes",
		status: "Active",
		expenses: 100000,
		sow: 0,
		start: new Date(2017,10,16),
		end: new Date(2018,10,16),
		billedExp: 10000,
		nonBilledExp: 3300,
		totalExp: 50000000,
		margin: 99,
		overExp: 1,
		activities: [{
			employee: {name: "Noah Jordan", employee_id: 12121212},
			activity: "Billable",
			rate: 13,
			fees: 1000,
			hourlyCost: 8,
			plannedHours: 15
		},
		{
			employee: {name: "Noah Gordan", employee_id: 12121212},
			activity: "Non - Billable",
			rate: 45,
			fees: 643,
			hourlyCost: 6,
			plannedHours: 23
		},
		{
			employee: {name: "Noah Bordan", employee_id: 12121212},
			activity: "Straight losses",
			rate: 0,
			fees: 2345,
			hourlyCost: 9,
			plannedHours: 764
		}]
	};

})();
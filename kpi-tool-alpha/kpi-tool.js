
//Import all dependencies
var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var http = require('http');
var db = require('./database');

//Create the express server
var app = express();

//Instruct the server to regconize JSON data
app.use(bodyParser.json());

//Set the directory path
app.use(express.static(__dirname + '/pages'));

console.log("dir: ", __dirname)

//Set the default route for the app and serve the file
app.get('/', function(request, response) {
	response.sendFile(__dirname + '/pages/kpi-tool.html')
});

//This GET returns a list of all employees with "Active Employee" status from the database
app.get('/employees', function(request, response) {
	var query = "SELECT CONCAT(emp_firstname, \" \", emp_lastname) AS name, employee_id FROM hs_hr_employee WHERE custom5 LIKE \"Active Employee\" ORDER BY name";
	db.executeQuery(query, function(err, rows, fields) {
		if (err) return response.status(500, "Error while performing query:\n" + err);
		else {
			return response.status(200).send(rows);
		};
	});
});

//The status variable is used to store the selected status which is posted after a dropdown selection is made
var status;

//This POST method allows the Angular app to share data with the server. In this case the data is the status selected from the dropdown menu.
app.post('/selectedStatus', function(request, response) {
	console.log("selected status:", request.body);
	status = request.body.status;
	console.log("status: " + status)
	console.log(typeof(status))
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	console.log(JSON.stringify(request.body));

	response.end(JSON.stringify(request.body));
});


//This GET method returns a list of customer objects based on the defined status. Each object has attributes: name, customer_id.
app.get('/curratedCustomers', function(request, response) {
	var query = "SELECT DISTINCT ohrm_customer.name, ohrm_customer.customer_id FROM ohrm_project INNER JOIN ohrm_customer ON ohrm_customer.customer_id = ohrm_project.customer_id WHERE ohrm_project.status = " + status;
	
	//If the selected status is "All", change query to return all customers.
	if (status == 3) {
		query = "SELECT DISTINCT ohrm_customer.name, ohrm_customer.customer_id FROM ohrm_project INNER JOIN ohrm_customer ON ohrm_customer.customer_id = ohrm_project.customer_id";
	}
	console.log(query);
	db.executeQuery(query, function(err, rows, fields) {
		if (err) return response.status(500, "Error while performing query:\n" + err);
		else {
			return response.status(200).send(rows);
		};
	});
});


//Server side variables to store the selected customers name and customer_id attributes.
var customer;
var customerId;

//This POST method shares the selected customer from the Angular app to the server.
app.post('/selectedCustomer', function(request, response) {
	console.log("selected customer: ", request.body);
	customerId = request.body.customer.customer_id;
	customer = request.body.customer.name;
	console.log("customer_id: " + customer)
	console.log(typeof(customer))
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	response.end(JSON.stringify(request.body));
});


//This GET method returns a list of projects assiciated with the selected customer and status
app.get('/curratedProjects', function(request, response) {
	
	//When a status is selected use it to query the DB accordingly.
	var query = "SELECT name AS project, project_id, status, start_date, end_date FROM ohrm_project WHERE customer_id = " + customerId + " AND status = " + status + " ORDER BY project";
	
	//If the selected status is "All" change the query.
	if (status == 3) {
		query = "SELECT ohrm_project.name AS project, ohrm_project.project_id, ohrm_project.status, ohrm_project.start_date, ohrm_project.end_date FROM ohrm_project WHERE ohrm_project.customer_id = " + customerId + " ORDER BY project";
	}
	
	console.log(query);
	db.executeQuery(query, function(err, rows, fields) {
		if (err) return response.status(500, "Error while performing query:\n" + err);
		else {
			return response.status(200).send(rows);
		};
	});
});

//Create an empty project object to store the projects attributes server side
var project = {};

app.post('/selectedProject', function(request, response) {
	console.log("request.body.project.status: ", request.body.project.status)

	//The statusString variable is used to change the status code from the database to a string to be displayed. E.g. from "1" to "Active"
	var statusString;
	//Switch changes the returned status to a string to be stored in the posted project
	switch (request.body.project.status){
		case 0: statusString = "Inactive"; break;
		case 1: statusString = "Active"; break;
		case 2: statusString = "Closed"; break;
	}

	//Build the first half of the project object with data from the ohrm_project table. The "customer" attribute is set from when the customer was first selected.
	//Initially "expected_expenses" "fixed" and "sow_total" are set to "default" values. If the attributes exist in the neos_project_data table they will be updated.
	project = {
		customer: customer,
		project: request.body.project.project,
		project_id: request.body.project.project_id,
		status: statusString,
		start_date: request.body.project.start_date,
		end_date: request.body.project.end_date,
		expected_expenses: 0,
		fixed: "None",
		sow_total: 0
	};

	console.log("server side project: ", project);
	//console.log("Customer: ", customer, " Project ID: ", projectId)
	response.end();
});

//This GET method assembles the final project object to be displayed in the dashboard.
app.get('/selectedProject', function(request, response) {

	//Use the current project objects "project_id" attribute to query neos_project_data
	var query = "SELECT neos_project_data.expected_expenses, neos_project_data.fixed, neos_project_data.sow_total FROM neos_project_data WHERE neos_project_data.project_id = " + project.project_id;
	console.log(query);

	console.log("Customer: ", project.customer, " Project ID: ", project.project_id)

	//Execute the SELECT query
	db.executeQuery(query, function(err, rows, fields) {
		if (err) return response.status(500, "Error while performing query:\n" + err);
		else {
			console.log("Rows: ", rows, " Length: ", rows.length)

			//If the query returns nothing i.e. there is no row in neos_project_data for the given project_id
			if (rows.length == 0) {

				//Create a new row
				query = "INSERT INTO neos_project_data (project_id) VALUES (" + project.project_id + ")";
				console.log(query)

				db.insertQuery(query, function(err, rows, fields) {
					if (err) return "Error while performing query:\n" + err;

					else {
						return 'success';
					};
				});




				//Return the project object as is. I.e. with default values for "expected_expenses" "fixed" and "sow_total". 
				rows[0] = project;
				return response.status(200).send(rows)
			}

			//If the query returns data
			else {

				//Set the projects "expected_expenses" attribute to the "expected_expenses" attribute returned from the query.
				project.expected_expenses = rows[0].expected_expenses;

				//If the "fixed" attribute returned is coded as "0" set the project objects value to "No" to display.
				if (rows[0].fixed == 0)
					project.fixed = "No";
				else
					project.fixed = "Yes";

				//Set the project objects "sow_total" to the returned value.
				project.sow_total = rows[0].sow_total;

				//Return the final object
				rows[0] = project;
				return response.status(200).send(rows);
			}
	
		};
	});
});

//Create a path for POSTing edits to a project
app.post('/projectEdits', function(request, response) {
	var edits = request.body.edits;
	console.log("edits: ", edits);
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";
	response.end();

	//This boolean reflects whether the values given to the server for status and fixed are valid
	var badQuery = false;
	var query = "";

	//These switches check the validity of the inputs
	switch (edits.status){
		case "Inactive": edits.status = 0; break;
		case "Active": edits.status = 1; break;
		case "Closed": edits.status = 2; break;
		default: badQuery = true; break;
	}
	switch (edits.fixed){
		case "No": edits.fixed = 0; break;
		case "Yes": edits.fixed = 1; break;
		default: badQuery = true; break;
	}

	console.log("edits1: ", edits);

	//If the inputs are valid set the query variable
	if (badQuery == false) {
		query = "UPDATE neos_project_data, ohrm_project SET ohrm_project.start_date = DATE(\"" + edits.start_date + "\"), ohrm_project.end_date = DATE(\"" + edits.end_date + "\"), ohrm_project.status = " + edits.status + ", neos_project_data.expected_expenses = " + edits.expected_expenses + ", neos_project_data.fixed = " + edits.fixed + ", neos_project_data.sow_total = " + edits.sow_total + " WHERE neos_project_data.project_id = ohrm_project.project_id AND ohrm_project.project_id = " + edits.project_id;
	}
	else {
		console.log("badquery");
	}
	

	console.log(query);

	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
});

//Define a path to GET activity rates
//This query is the longest in length an execution time
//It's a pretty good one, took me like all day...
app.get('/activityRates', function(request, response) {
	var query = "SELECT t1.project_id, t1.employee, t1.employee_id, t1.hashKey, t1.hours, t2.rate, t1.date, MAX(t4.loaded_cost) AS hourly_cost, (MAX(t4.loaded_cost) * t1.hours) AS loaded_cost, (t1.hours * t2.rate) AS fees, t3.name AS activity, t3.activity_id FROM (SELECT CONCAT(ohrm_timesheet_item.employee_id, \" \", ohrm_timesheet_item.activity_id, \" \", ohrm_timesheet_item.project_id) AS hashKey, CONCAT(hs_hr_employee.emp_firstname, \" \", hs_hr_employee.emp_lastname) AS employee, ohrm_timesheet_item.project_id, SUM(ohrm_timesheet_item.duration)/3600 AS hours, ohrm_timesheet_item.employee_id, ohrm_timesheet_item.activity_id, ohrm_timesheet_item.date FROM ohrm_timesheet_item INNER JOIN hs_hr_employee ON ohrm_timesheet_item.employee_id = hs_hr_employee.employee_id GROUP BY hashKey) t1 LEFT JOIN (SELECT customer_id, project_id, activity_id, employee_id, rate, CONCAT(employee_id, \" \", activity_id, \" \", project_id) AS hashKey FROM neos_activity_rates) t2 ON t1.hashKey = t2.hashKey LEFT JOIN ohrm_project_activity t3 ON t3.activity_id = t1.activity_id LEFT JOIN neos_employee_cost t4 ON t4.employee_id = t1.employee_id AND t4.begin_date <= t1.date WHERE t1.project_id = " + project.project_id + " GROUP BY t1.hashKey"

	console.log(query);
	db.executeQuery(query, function(err, rows, fields) {
		if (err) return response.status(500, "Error while performing query:\n" + err);
		else {
			return response.status(200).send(rows);
		};
	});
});

//Server-side variable to store the employee id
var employeeId;

//A path to POST the employee selected in the dropdown on the employee page
app.post('/selectedEmployee', function(request, response) {
	console.log("selected employee: ", request.body);
	employeeId = request.body.employee_id;
	console.log("employee: " + employeeId)
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";
	response.end();
});

//A path to GET employee costs
app.get('/employeeCosts', function(request, response) {
	var query = "SELECT * FROM neos_employee_cost WHERE employee_id = " + employeeId;

	console.log(query);
	db.executeQuery(query, function(err, rows, fields) {
		if (err) return response.status(500, "Error while performing query:\n" + err);
		else {
			return response.status(200).send(rows);
		};
	});
});



/*
SELECT t1.project_id, t1.employee, t1.employee_id, t1.hashKey, t1.hours, t2.rate, t1.date, MAX(t4.loaded_cost) as hourly_cost, (MAX(t4.loaded_cost) * t1.hours) AS loaded_cost, t3.name AS activity, t3.activity_id FROM 
	(SELECT CONCAT(ohrm_timesheet_item.employee_id, " ", ohrm_timesheet_item.activity_id, " ", ohrm_timesheet_item.project_id) AS hashKey, CONCAT(hs_hr_employee.emp_firstname, " ", hs_hr_employee.emp_lastname) AS employee, ohrm_timesheet_item.project_id, SUM(ohrm_timesheet_item.duration)/3600 AS hours, ohrm_timesheet_item.employee_id, ohrm_timesheet_item.activity_id, ohrm_timesheet_item.date FROM ohrm_timesheet_item INNER JOIN hs_hr_employee ON ohrm_timesheet_item.employee_id = hs_hr_employee.employee_id GROUP BY hashKey) t1 
		LEFT JOIN (SELECT customer_id, project_id, activity_id, employee_id, rate, CONCAT(employee_id, " ", activity_id, " ", project_id) AS hashKey FROM neos_activity_rates) t2 
        ON t1.hashKey = t2.hashKey
        
        LEFT JOIN ohrm_project_activity t3
        ON t3.activity_id = t1.activity_id
        
        LEFT JOIN neos_employee_cost t4
        ON t4.employee_id = t1.employee_id AND t4.begin_date <= t1.date

WHERE t1.project_id = 6

GROUP BY t1.hashKey

*/

//A variable to store a newCost object passed to the server
var newCost = {};

//A path to POST and INSERT new employee costs
app.post('/newCost', function(request, response) {
	console.log("newCost: ", request.body.newCost);
	newCost = request.body.newCost;
	console.log("employee_id: " + newCost.employee_id)
	console.log("cost: " + newCost.cost)
	console.log("begin_date: " + newCost.begin_date)
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	var query = "INSERT INTO neos_employee_cost (employee_id, begin_date, loaded_cost) VALUES (" + newCost.employee_id + ", " + "DATE(\"" + newCost.begin_date + "\"), " + newCost.loaded_cost + ")";

	console.log(query);

	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
	response.end();
});

//A path to DELETE employee costs from the database
app.post('/deleteCost', function(request, response) {
	var costToDelete = request.body.cost;
	console.log("cost: ", request.body.cost);
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	var query = "DELETE FROM neos_employee_cost WHERE employee_id = " + costToDelete.employee_id + " AND begin_date = DATE(\"" + costToDelete.begin_date + "\") AND loaded_cost = " + costToDelete.loaded_cost;

	console.log(query);

	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
	response.end();
	
});

//A path to UPDATE rates 
app.post('/updateRate', function(request, response) {
	var rateToUpdate = request.body.activity;
	console.log("rateObj: ", request.body.activity);
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	var query = "UPDATE neos_activity_rates SET rate = " + rateToUpdate.rate + " WHERE employee_id = " + rateToUpdate.employee_id + " AND project_id = " + rateToUpdate.project_id + " AND activity_id = " + rateToUpdate.activity_id;

	console.log(query);

	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
	response.end();
	
});

//A path to GET project expenses
app.get('/projectExpenses', function(request, response) {
	var query = "SELECT * FROM neos_project_invoice WHERE project_id = " + project.project_id;

	console.log(query);
	db.executeQuery(query, function(err, rows, fields) {
		if (err) return response.status(500, "Error while performing query:\n" + err);
		else {
			//If the project has no associated expenses set the rows variable to just return the project_id
			//and notify the front-end that there are no associated expenses.
			if (rows.length == 0) {
				var rows = [
				{
					project_id: project.project_id, 
					none: true
				}];
			}

			//Else just append the first object with a notification that there was data returned
			else {
				rows[0].none = false;
			}

			//Iterate through the rows and change the code for "billed" attribute to a string for the view.
			for (var i = rows.length - 1; i >= 0; i--) {
				switch (rows[i].billed) {
					case 0: rows[i].billed = "No"; break;
					case 1: rows[i].billed = "Yes"; break;
				}
			}
			
			console.log("rows: ", rows)
			return response.status(200).send(rows);
		};
	});
});

//A path to POST newExpense objects to the server
app.post('/newExpense', function(request, response) {
	var newExpense = request.body.expense;
	console.log("expense: ", request.body.expense);
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	//Change the string from the view back to the code for the database
	switch(newExpense.billed) {
		case "Yes": newExpense.billed = 1; break;
		case "No": newExpense.billed = 0; break;
	}


	console.log("newExpense: ", newExpense);

	var query = "INSERT INTO neos_project_invoice (expense_amount, expense_description, expense_date, id, project_id, billed) SELECT " + newExpense.expense_amount + ", \"" + newExpense.expense_description + "\", DATE(\"" + newExpense.expense_date + "\"), (MAX(id) + 1), " + newExpense.project_id + ", " + newExpense.billed + " FROM neos_project_invoice";

	console.log(query);

	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
	response.end();
	
});

//A path to delete expense objects from the database
app.post('/deleteExpense', function(request, response) {
	var expenseToDelete = request.body.expense;
	console.log("expenseToDelete: ", request.body.expense);
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	var query = "DELETE FROM neos_project_invoice WHERE id = " + expenseToDelete.id;
	console.log(query);

	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
	response.end();
	
});

//A path to update a given expense object
app.post('/updateExpense', function(request, response) {
	var expenseToUpdate = request.body.expense;
	console.log("expenseToUpdate: ", request.body.expense);
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	//Change the string from the view to an int for the database
	switch(expenseToUpdate.billed) {
		case "Yes": expenseToUpdate.billed = 1; break;
		case "No": expenseToUpdate.billed = 0; break;
	}

	var query = "UPDATE neos_project_invoice SET expense_amount = " + expenseToUpdate.expense_amount + ", expense_description = \"" + expenseToUpdate.expense_description + "\", expense_date = DATE(\"" + expenseToUpdate.expense_date + "\"), billed = " + expenseToUpdate.billed + " WHERE id = " + expenseToUpdate.id;
	console.log(query);

	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
	response.end();
	
});



console.log("Connected at port 3030");
app.listen(3030);

//Import all dependencies
var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var http = require('http');
var db = require('./config/database');
var session = require('client-sessions');
var passwordHash = require('password-hash');
var serveStatic = require('serve-static');

//Create the express server
var app = express();

var http = require('http'); 
var httpServer = http.Server(app);

//Instruct the server to regconize JSON data
app.use(bodyParser.json());

app.use(session({
	cookieName: 'session',
	secret: '1l0v3c!0I#EnKrY9N*0N',
	duration: 15 * 60 * 1000,
	activeDuration: 15 * 60 * 1000
}))

//Set the directory path
app.use(serveStatic(__dirname + '/public/pages'));

console.log("dir: ", __dirname)

app.use('/modules', express.static(__dirname + '/public/js/modules'));

app.use('/js', express.static(__dirname + '/public/js'));

app.use('/img', express.static(__dirname + '/public/img'));

app.use('/styles', express.static(__dirname + '/public/styles'));

app.use('/fonts', express.static(__dirname + '/public/styles/fonts'));


app.get('/authenticate', function(request, response) {
    var form_username = mysql.escape(request.query.username);
    form_username = form_username.replace(/'/g,"")
    var form_password = mysql.escape(request.query.password);
    form_password = form_password.replace(/'/g,"")
    var fullquery = 'SELECT username, password, admin FROM neos_user_management'
    var checkPassword = false;

   	if(form_password != "" && form_username != ""){    
        db.executeQuery(fullquery, function(err, rows, fields) {
            if (!err) {
                for(var i = 0; i < rows.length; i++) {
                    if(rows[i]["username"] == form_username && passwordHash.verify(form_password, rows[i]["password"])) {
                        checkPassword = true;
                        request.session.user = form_username; //create session
                        request.session.password = form_password;
                        request.session.admin = rows[i]["admin"]
                        return response.status(200).send(checkPassword);
                    }
                }
                return response.status(200).send(checkPassword);
            } 
            else {
                response.status(500).send({error: 'Error while performing fees query.\n'+err});
            }
        });
   	}    
});

//Set the default route for the app and serve the file
app.get('/dashboard', function(request, response) {
	if (request.session && request.session.user){
		response.sendFile(__dirname + '/public/pages/kpi-tool.html')
	}
	else {
		response.redirect('/login')
	}
});

app.get('/login', function(request, response){
    if(request.session && request.session.user){
        response.redirect('/')    
    }
    else{
        response.sendFile((__dirname + '/public/pages/login.html'));
    }
});

app.get('/logout', function(request, response){
    request.session.destroy();
    response.end();
});

app.get('/user', function(request, response) {
	response.send({username: request.session.user, admin: request.session.admin})
})


app.post('/changePassword', function(request, response) {
	response.setHeader('Content-Type', 'application/json');
	if (request.body.data.current == request.session.password) {
		var update = request.body.data.new.replace(/'/g,"")
		var query = "UPDATE neos_user_management SET password = \"" + passwordHash.generate(update) + "\" WHERE username = \"" + request.session.user + "\"";
		db.insertQuery(query, function(err, rows, fields) {
			if (err) {
				return response.status(500).send({error: "Error while performing query:\n" + err, success: false});
			}
			else {
				request.session.password = update;
				return response.status(200).send({error: false, success: true});
			};
		});
	}
	else {
		return response.send({error: true, success: false})
	}
	request.body.message = "Thanks for the data";
});

app.post('/createNewUser', function(request, response) {
	response.setHeader('Content-Type', 'application/json');
	var newUser = request.body.newUser;
	if ((newUser.adminPassword == request.session.password) && request.session.admin) {
		var query = "SELECT username FROM neos_user_management WHERE username LIKE \"" + newUser.username + "\""
		db.executeQuery(query, function(err, rows, fields) {
			if (err) return response.status(500).send({error: err, code: 0});
			else {
				if (rows.length != 0) {
					return response.status(409).send({error: "Username already exists.", code: 1});
				}
				//Actually insert the new user
				else {
					var password = passwordHash.generate(newUser.password)
					query = "INSERT INTO neos_user_management (username, password, admin) VALUES (\"" + newUser.username + "\", \"" + password + "\", " + newUser.admin + ")"
					db.insertQuery(query, function(err, rows, fields) {
						if (err) return response.status(500).send({error: err, code: 0});
						else {
							return response.status(200).end();
						};
					});
				}
			};
		});
	}
	else if (request.session.admin) {
		return response.status(403).send({error: "403 Forbidden", code: 2});
	}
	else {
		request.session.destroy();
		return response.status(401).send({error: "401 Unauthorized", code: 3});
	}
});


app.get('/userAccounts', function(request, response) {
	var query = "SELECT username, admin, id FROM neos_user_management WHERE username != \"" + request.session.user + "\"";
	db.executeQuery(query, function(err, rows, fields) {
		if (err) return response.status(500).send({error: err});
		else {
			return response.status(200).send(rows);
		};
	});
});

app.post('/modifiedUser', function(request, response) {
	response.setHeader('Content-Type', 'application/json');
	var modifiedUser = request.body.modifiedUser;
	if ((modifiedUser.adminPassword == request.session.password) && request.session.admin) {
		var query = "SELECT * FROM neos_user_management WHERE id = " + modifiedUser.id
		db.executeQuery(query, function(err, rows, fields) {
			if (err) return response.status(500).send({error: err, code: 0});
			else {
				if (rows.length == 0) {
					return response.status(404).send({error: "User not found.", code: 3});
				}
				//Run checks on all attrs
				else {
					var originalUser = rows[0];

					//if passwords don't match AND passwords not null or ""
					if (modifiedUser.password) {
						modifiedUser.password = passwordHash.generate(modifiedUser.password)
					}
					else {
						modifiedUser.password = originalUser.password
					}
					//If usernames don't match and username's not null or "" IE change username
					if ((modifiedUser.username != originalUser.username) && modifiedUser.username) {
						//check that the username doesn't exist in the DB
						query = "SELECT username FROM neos_user_management WHERE username LIKE \"" + modifiedUser.username + "\""
						db.executeQuery(query, function(err, rows, fields) {
							if (err) return response.status(500).send({error: err, code: 0});
							else if (rows.length != 0) {
								return response.status(409).send({error: "Username already exists.", code: 1});
							}
							else {
								query = "UPDATE neos_user_management SET username = \"" + modifiedUser.username + "\", password = \"" + modifiedUser.password + "\", admin = " + modifiedUser.admin + " WHERE id = " + modifiedUser.id
								db.insertQuery(query, function(err, rows, fields) {
									if (err) return response.status(500).send({error: err, code: 0});
									else {
										return response.status(200).end()
									}

								})
							}
						})
					}
					else {
						query = "UPDATE neos_user_management SET password = \"" + modifiedUser.password + "\", admin = " + modifiedUser.admin + " WHERE id = " + modifiedUser.id
						db.insertQuery(query, function(err, rows, fields) {
							if (err) return response.status(500).send({error: err, code: 0});
							else {
								return response.status(200).end()
							}
						})
					}
				}
			};
		});
	}
	else if (request.session.admin) {
		return response.status(403).send({error: "403 Forbidden", code: 2});
	}
	else {
		request.session.destroy();
		return response.status(401).send({error: "401 Unauthorized", code: 4});
	}
	request.body.message = "Thanks for the data";
});










//This GET returns a list of all employees with "Active Employee" status from the database
app.get('/employees', function(request, response) {
	var query = "SELECT CONCAT(emp_firstname, \" \", emp_lastname) AS name, employee_id FROM hs_hr_employee WHERE custom5 LIKE \"Active Employee\" ORDER BY name";
	db.executeQuery(query, function(err, rows, fields) {
		if (err) return response.status(500, "Error while performing query:\n" + err);
		else {
			return response.status(200).send(rows);
		};
	});
});

//The status variable is used to store the selected status which is posted after a dropdown selection is made
var status;

//This POST method allows the Angular app to share data with the server. In this case the data is the status selected from the dropdown menu.
app.post('/selectedStatus', function(request, response) {
	status = request.body.status;
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";
	response.end();
});


//This GET method returns a list of customer objects based on the defined status. Each object has attributes: name, customer_id.
app.get('/curratedCustomers', function(request, response) {
	var query = "SELECT DISTINCT ohrm_customer.name, ohrm_customer.customer_id FROM ohrm_project INNER JOIN ohrm_customer ON ohrm_customer.customer_id = ohrm_project.customer_id WHERE ohrm_project.status = " + status;
	
	//If the selected status is "All", change query to return all customers.
	if (status == 3) {
		query = "SELECT DISTINCT ohrm_customer.name, ohrm_customer.customer_id FROM ohrm_project INNER JOIN ohrm_customer ON ohrm_customer.customer_id = ohrm_project.customer_id";
	}
	db.executeQuery(query, function(err, rows, fields) {
		if (err) {
			return response.status(200).send(err)
		}
		else {
			return response.status(200).send(rows);
		};
	});
});


//Server side variables to store the selected customers name and customer_id attributes.
var customer;
var customerId;

//This POST method shares the selected customer from the Angular app to the server.
app.post('/selectedCustomer', function(request, response) {
	customerId = request.body.customer.customer_id;
	customer = request.body.customer.name;
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";
	response.end();
});


//This GET method returns a list of projects assiciated with the selected customer and status
app.get('/curratedProjects', function(request, response) {
	
	//When a status is selected use it to query the DB accordingly.
	var query = "SELECT name AS project, project_id, status, start_date, end_date FROM ohrm_project WHERE customer_id = " + customerId + " AND status = " + status + " ORDER BY project";
	
	//If the selected status is "All" change the query.
	if (status == 3) {
		query = "SELECT ohrm_project.name AS project, ohrm_project.project_id, ohrm_project.status, ohrm_project.start_date, ohrm_project.end_date FROM ohrm_project WHERE ohrm_project.customer_id = " + customerId + " ORDER BY project";
	}
	db.executeQuery(query, function(err, rows, fields) {
		if (err) {
			return response.status(200).send(err)
		}
		else {
			return response.status(200).send(rows);
		};
	});
});

//Create an empty project object to store the projects attributes server side
var project = {};

app.post('/selectedProject', function(request, response) {

	//The statusString variable is used to change the status code from the database to a string to be displayed. E.g. from "1" to "Active"
	var statusString;
	//Switch changes the returned status to a string to be stored in the posted project
	switch (request.body.project.status){
		case 0: statusString = "Inactive"; break;
		case 1: statusString = "Active"; break;
		case 2: statusString = "Closed"; break;
	}

	//Build the first half of the project object with data from the ohrm_project table. The "customer" attribute is set from when the customer was first selected.
	//Initially "expected_expenses" "fixed" and "sow_total" are set to "default" values. If the attributes exist in the neos_project_data table they will be updated.
	project = {
		customer: customer,
		project: request.body.project.project,
		project_id: request.body.project.project_id,
		status: statusString,
		start_date: request.body.project.start_date,
		end_date: request.body.project.end_date,
		expected_expenses: 0,
		fixed: "None",
		sow_total: 0
	};
	response.end();
});

//This GET method assembles the final project object to be displayed in the dashboard.
app.get('/selectedProject', function(request, response) {

	//Use the current project objects "project_id" attribute to query neos_project_data
	var query = "SELECT neos_project_data.expected_expenses, neos_project_data.fixed, neos_project_data.sow_total FROM neos_project_data WHERE neos_project_data.project_id = " + project.project_id;

	//Execute the SELECT query
	db.executeQuery(query, function(err, rows, fields) {
		if (err) {
			return response.status(200).send(err)
		}
		else {

			//If the query returns nothing i.e. there is no row in neos_project_data for the given project_id
			if (rows.length == 0) {

				//Create a new row
				query = "INSERT INTO neos_project_data (project_id) VALUES (" + project.project_id + ")";
				db.insertQuery(query, function(err, rows, fields) {
					if (err) return "Error while performing query:\n" + err;

					else {
						return 'success';
					};
				});




				//Return the project object as is. I.e. with default values for "expected_expenses" "fixed" and "sow_total". 
				rows[0] = project;
				return response.status(200).send(rows)
			}

			//If the query returns data
			else {

				//Set the projects "expected_expenses" attribute to the "expected_expenses" attribute returned from the query.
				project.expected_expenses = rows[0].expected_expenses;

				//If the "fixed" attribute returned is coded as "0" set the project objects value to "No" to display.
				if (rows[0].fixed == 0)
					project.fixed = "No";
				else
					project.fixed = "Yes";

				//Set the project objects "sow_total" to the returned value.
				project.sow_total = rows[0].sow_total;

				//Return the final object
				rows[0] = project;
				return response.status(200).send(rows);
			}
	
		};
	});
});

app.get('/projectHours', function(request, response) {
	var query = "SELECT *, STR_TO_DATE(CONCAT(t1.yearweek, \'Sunday\'), \'%X%V %W\') AS sunday FROM (SELECT SUM(duration/3600) AS hours, date, YEARWEEK(date) AS yearweek, employee_id FROM ohrm_timesheet_item WHERE project_id = " + project.project_id + " GROUP BY timesheet_id) t1 LEFT JOIN (SELECT CONCAT(emp_firstname, \" \", emp_lastname) AS employee, employee_id FROM hs_hr_employee) t2 ON t1.employee_id = t2.employee_id"
	db.executeQuery(query, function(err, rows, fields) {
		if (err) return response.status(200).send("Error while performing query:\n" + err);
		else {
			return response.status(200).send(rows);
		};
	});
});

//Create a path for POSTing edits to a project
app.post('/projectEdits', function(request, response) {
	var edits = request.body.edits;
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";
	response.end();

	//This boolean reflects whether the values given to the server for status and fixed are valid
	var badQuery = false;
	var query = "";

	//These switches check the validity of the inputs
	switch (edits.status){
		case "Inactive": edits.status = 0; break;
		case "Active": edits.status = 1; break;
		case "Closed": edits.status = 2; break;
		default: badQuery = true; break;
	}
	switch (edits.fixed){
		case "No": edits.fixed = 0; break;
		case "Yes": edits.fixed = 1; break;
		default: badQuery = true; break;
	}

	//If the inputs are valid set the query variable
	if (badQuery == false) {
		query = "UPDATE neos_project_data, ohrm_project SET ohrm_project.start_date = DATE(\"" + edits.start_date + "\"), ohrm_project.end_date = DATE(\"" + edits.end_date + "\"), ohrm_project.status = " + edits.status + ", neos_project_data.expected_expenses = " + edits.expected_expenses + ", neos_project_data.fixed = " + edits.fixed + ", neos_project_data.sow_total = " + edits.sow_total + " WHERE neos_project_data.project_id = ohrm_project.project_id AND ohrm_project.project_id = " + edits.project_id;
	}
	else {
	}

	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
});

//Define a path to GET activity rates
//This query is the longest in length an execution time
//It's a pretty good one, took me like all day...
app.get('/activityRates', function(request, response) {
	//var query = "SELECT t1.project_id, t1.employee, t1.employee_id, t1.hashKey, t1.hours, t2.rate, t1.date, MAX(t4.loaded_cost) AS hourly_cost, (MAX(t4.loaded_cost) * t1.hours) AS loaded_cost, (t1.hours * t2.rate) AS fees, t3.name AS activity, t3.activity_id FROM (SELECT CONCAT(ohrm_timesheet_item.employee_id, \" \", ohrm_timesheet_item.activity_id, \" \", ohrm_timesheet_item.project_id) AS hashKey, CONCAT(hs_hr_employee.emp_firstname, \" \", hs_hr_employee.emp_lastname) AS employee, ohrm_timesheet_item.project_id, SUM(ohrm_timesheet_item.duration)/3600 AS hours, ohrm_timesheet_item.employee_id, ohrm_timesheet_item.activity_id, ohrm_timesheet_item.date FROM ohrm_timesheet_item INNER JOIN hs_hr_employee ON ohrm_timesheet_item.employee_id = hs_hr_employee.employee_id GROUP BY hashKey) t1 LEFT JOIN (SELECT customer_id, project_id, activity_id, employee_id, rate, CONCAT(employee_id, \" \", activity_id, \" \", project_id) AS hashKey FROM neos_activity_rates) t2 ON t1.hashKey = t2.hashKey LEFT JOIN ohrm_project_activity t3 ON t3.activity_id = t1.activity_id LEFT JOIN neos_employee_cost t4 ON t4.employee_id = t1.employee_id AND t4.begin_date <= t1.date WHERE t1.project_id = " + project.project_id + " GROUP BY t1.hashKey"

	var query = "SELECT t4.*, (t4.hourly_cost * t4.hours) AS loaded_cost, t5.rate, (t4.hours * t5.rate) AS fees, t6.employee, t7.activity FROM (SELECT t3.employee_id, t3.project_id, t3.activity_id, t3.hashKey, t3.max_cost AS hourly_cost, t3.start_date, MAX(t3.date) AS end_date, (SUM(t3.duration)/3600) AS hours FROM (SELECT t1.project_id, t1.activity_id, t1.date, CONCAT(t1.project_id, \" \", t1.employee_id, \" \", t1.activity_id, \" \", MAX(t2.loaded_cost)) AS hash1, CONCAT(t1.employee_id, \" \", t1.activity_id, \" \", t1.project_id) AS hashKey, MAX(t2.loaded_cost) AS max_cost, MAX(t2.begin_date) AS start_date, duration, t1.employee_id FROM ohrm_timesheet_item t1 LEFT JOIN neos_employee_cost t2 ON t1.employee_id = t2.employee_id AND t2.begin_date <= t1.date WHERE t1.project_id = " + project.project_id + " GROUP BY t1.timesheet_item_id) t3 GROUP BY CASE WHEN t3.hash1 IS NULL THEN t3.hashKey ELSE t3.hash1 END) t4 LEFT JOIN (SELECT *, CONCAT(employee_id, \" \", activity_id, \" \", project_id) AS hashKey FROM neos_activity_rates) t5 ON t4.hashKey = t5.hashKey LEFT JOIN (SELECT CONCAT(emp_firstname, \" \", emp_lastname) as employee, employee_id FROM hs_hr_employee) t6 ON t4.employee_id = t6.employee_id LEFT JOIN (SELECT activity_id, name AS activity FROM ohrm_project_activity) t7 ON t7.activity_id = t4.activity_id"
	db.executeQuery(query, function(err, rows, fields) {
		if (err) {
			return response.status(200).send(err)
		}
		else {
			return response.status(200).send(rows);
		};
	});
});

//Server-side variable to store the employee id
var employeeId;

//A path to POST the employee selected in the dropdown on the employee page
app.post('/selectedEmployee', function(request, response) {
	employeeId = request.body.employee_id;
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";
	response.end();
});

//A path to GET employee costs
app.get('/employeeCosts', function(request, response) {
	var query = "SELECT * FROM neos_employee_cost WHERE employee_id = " + employeeId;
	db.executeQuery(query, function(err, rows, fields) {
		if (err) {
			return response.status(200).send(err)
		}
		else {
			return response.status(200).send(rows);
		};
	});
});

//A variable to store a newCost object passed to the server
var newCost = {};

//A path to POST and INSERT new employee costs
app.post('/newCost', function(request, response) {
	newCost = request.body.newCost;
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	var query = "INSERT INTO neos_employee_cost (employee_id, begin_date, loaded_cost) VALUES (" + newCost.employee_id + ", " + "DATE(\"" + newCost.begin_date + "\"), " + newCost.loaded_cost + ")";
	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
	response.end();
});

//A path to DELETE employee costs from the database
app.post('/deleteCost', function(request, response) {
	var costToDelete = request.body.cost;
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	var query = "DELETE FROM neos_employee_cost WHERE employee_id = " + costToDelete.employee_id + " AND begin_date = DATE(\"" + costToDelete.begin_date + "\") AND loaded_cost = " + costToDelete.loaded_cost;

	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
	response.end();
	
});

//A path to UPDATE rates 
app.post('/updateRate', function(request, response) {
	var rateToUpdate = request.body.activity;
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	if (typeof(rateToUpdate.rate) != "number") {
		rateToUpdate.rate = 0;
	}

	var query = "UPDATE neos_activity_rates SET rate = " + rateToUpdate.rate + " WHERE employee_id = " + rateToUpdate.employee_id + " AND project_id = " + rateToUpdate.project_id + " AND activity_id = " + rateToUpdate.activity_id;
	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
	response.end();
	
});

//A path to GET project expenses
app.get('/projectExpenses', function(request, response) {
	var query = "SELECT * FROM neos_project_invoice WHERE project_id = " + project.project_id;
	db.executeQuery(query, function(err, rows, fields) {
		if (err) {
			return response.status(200).send(err)
		}
		else {
			//If the project has no associated expenses set the rows variable to just return the project_id
			//and notify the front-end that there are no associated expenses.
			if (rows.length == 0) {
				var rows = [
				{
					project_id: project.project_id, 
					none: true
				}];
			}

			//Else just append the first object with a notification that there was data returned
			else {
				rows[0].none = false;
			}

			//Iterate through the rows and change the code for "billed" attribute to a string for the view.
			for (var i = rows.length - 1; i >= 0; i--) {
				switch (rows[i].billed) {
					case 0: rows[i].billed = "No"; break;
					case 1: rows[i].billed = "Yes"; break;
				}
			}
			return response.status(200).send(rows);
		};
	});
});

//A path to POST newExpense objects to the server
app.post('/newExpense', function(request, response) {
	var newExpense = request.body.expense;
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	//Change the string from the view back to the code for the database
	switch(newExpense.billed) {
		case "Yes": newExpense.billed = 1; break;
		case "No": newExpense.billed = 0; break;
	}

	var query = "INSERT INTO neos_project_invoice (expense_amount, expense_description, expense_date, id, project_id, billed) SELECT " + newExpense.expense_amount + ", \"" + newExpense.expense_description + "\", DATE(\"" + newExpense.expense_date + "\"), (MAX(id) + 1), " + newExpense.project_id + ", " + newExpense.billed + " FROM neos_project_invoice";

	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
	response.end();
	
});

//A path to delete expense objects from the database
app.post('/deleteExpense', function(request, response) {
	var expenseToDelete = request.body.expense;
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	var query = "DELETE FROM neos_project_invoice WHERE id = " + expenseToDelete.id;

	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
	response.end();
	
});

//A path to update a given expense object
app.post('/updateExpense', function(request, response) {
	var expenseToUpdate = request.body.expense;
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";

	//Change the string from the view to an int for the database
	switch(expenseToUpdate.billed) {
		case "Yes": expenseToUpdate.billed = 1; break;
		case "No": expenseToUpdate.billed = 0; break;
	}

	var query = "UPDATE neos_project_invoice SET expense_amount = " + expenseToUpdate.expense_amount + ", expense_description = \"" + expenseToUpdate.expense_description + "\", expense_date = DATE(\"" + expenseToUpdate.expense_date + "\"), billed = " + expenseToUpdate.billed + " WHERE id = " + expenseToUpdate.id;

	db.insertQuery(query, function(err, rows, fields) {
		if (err) return "Error while performing query:\n" + err;
		else {
			return 'success';
		};
	});
	response.end();
	
});

//======================================TIME MGMT QUERIES========================================================

//Get all employees including inactive
app.get('/allEmployees', function(request, response) {
	
	var query = "SELECT CONCAT(emp_firstname, \" \", emp_lastname) AS name, employee_id FROM hs_hr_employee ORDER BY name"
	db.executeQuery(query, function(err, rows, fields) {
		if (err) return response.status(500, "Error while performing query:\n" + err);
		else {
			return response.status(200).send(rows);
		};
	});
});

var selectedEmployee1;

app.post('/selectedEmployee1', function(request, response) {
	selectedEmployee1 = request.body.employee;
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";
	response.end();
});

var payPeriod;

app.post('/selectedPeriod', function(request, response) {
	payPeriod = request.body.payPeriod;
	response.setHeader('Content-Type', 'application/json');
	request.body.message = "Thanks for the data";
	response.end();
});

app.get('/curratedTimesheets', function(request, response) {
	
	var query = "SELECT * FROM (SELECT *, CONCAT(t1.timesheet_id, \" \", t1.customer_id) AS hash_key_2 FROM (SELECT t1.*, t3.employee, t2.total_hours, t4.project, t4.customer_id, t5.activity, (t1.hours_per_activity/t2.total_hours) * 100 AS percent_time, t5.billable, t7.name AS customer FROM (SELECT timesheet_id, date, project_id, employee_id, activity_id, SUM(duration/3600) AS hours_per_activity, YEARWEEK(date) as yearweek, CONCAT(timesheet_id, \" \", project_id, \" \", activity_id) AS hash_key FROM ohrm_timesheet_item WHERE (date BETWEEN DATE(\"" + payPeriod.startDate + "\") AND DATE(\"" + payPeriod.endDate + "\")) AND employee_id = " + selectedEmployee1.employee_id + " GROUP BY CONCAT(timesheet_id, \" \", project_id, \" \", activity_id)) t1 LEFT JOIN (SELECT timesheet_id, SUM(duration/3600) AS total_hours FROM ohrm_timesheet_item WHERE (date BETWEEN DATE(\"" + payPeriod.startDate + "\") AND DATE(\"" + payPeriod.endDate + "\")) GROUP BY timesheet_id) t2 ON t1.timesheet_id = t2.timesheet_id LEFT JOIN (SELECT CONCAT(emp_firstname, \" \", emp_lastname) AS employee, employee_id FROM hs_hr_employee) t3 ON t1.employee_id = t3.employee_id LEFT JOIN (SELECT name AS project, project_id, customer_id FROM ohrm_project) t4 ON t1.project_id = t4.project_id LEFT JOIN (SELECT ohrm_project_activity.name AS activity, activity_id, IF((ohrm_project_activity.name LIKE \"%billable%\" AND ohrm_project_activity.name LIKE \"%non%\" OR ohrm_project.customer_id = 8), 0, 1) AS billable FROM ohrm_project_activity INNER JOIN ohrm_project ON ohrm_project.project_id = ohrm_project_activity.project_id) t5 ON t1.activity_id = t5.activity_id LEFT JOIN ohrm_customer t7 ON t7.customer_id = t4.customer_id) t1) t1 LEFT JOIN (SELECT t1.*, t2.customer_id, CONCAT(t1.timesheet_id, \" \", t2.customer_id) AS hash_key, SUM(duration/3600) AS hours_per_customer FROM ohrm_timesheet_item t1 LEFT JOIN ohrm_project t2 ON t1.project_id = t2.project_id WHERE (date BETWEEN DATE(\"" + payPeriod.startDate + "\") AND DATE(\"" + payPeriod.endDate + "\")) GROUP BY CONCAT(t1.timesheet_id, \" \", t2.customer_id)) t6 ON t6.hash_key = t1.hash_key_2"
	
	db.executeQuery(query, function(err, rows, fields) {
		if (err) return response.status(200).send("Error while performing query:\n" + err);
		else {
			return response.status(200).send(rows);
		};
	});
});



console.log("Connected at port 3020");
app.listen(3020);
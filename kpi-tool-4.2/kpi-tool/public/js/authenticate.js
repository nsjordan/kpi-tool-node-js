
var quickURL = "kpi.neosllc.com:3020" //uncomment for go live
//var quickURL = "localhost:3030" //comment out for go live

$(document).ready(function(){

    $("#login").click(function(){
        var username = $("#username").val();
        username = username.replace(/(<([^>]+)>)/ig, "");
        username = username.toLowerCase();
        var password = $("#password").val();    
        
        if(username != "" && password != "" && username.match(/[A-Za-z]/)){                            
            callDatabase(username,password)
        }    
        else{
                $('#alert').text("Unsucessful login: Please enter Username and/or Password")
        }    
    })

    $("#password").keydown(function(key){
        if (key.keyCode == 13) {
            var username = $("#username").val();
            username = username.replace(/(<([^>]+)>)/ig, "");
            username = username.toLowerCase();
            var password = $("#password").val();    
            
            if(username != "" && password != "" && username.match(/[A-Za-z]/)){                            
                callDatabase(username,password)
            }    
            else{
                    $('#alert').text("Unsucessful login: Please enter Username and/or Password")
            }
        }       
    })
    
    function callDatabase(user, pass){
        isValid = false;
        return $.ajax({ 
            url: 'http://' + quickURL + '/authenticate', 
            type: 'GET', 
            data: { 
                username:user,
                password:pass 
            }, 
            success: function(response_data) { 
                isValid = response_data;
                redirect(isValid); 
            } 
        });
    }

    function redirect(is_valid){
        if (is_valid == true ){
            $('#alert').text("");
            window.location.href = 'http://' + quickURL + '/dashboard';
        }
        else{
            $('#alert').text("Unsucessful login: Incorrect Username or Password")
        }
    }

})
